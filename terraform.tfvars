region                      = "eu-west-3"
cidr_vpc                    = "10.0.0.0/16"
availability_zones          = ["eu-west-3a", "eu-west-3b"]
cidr_public_subnets         = ["10.0.0.0/19", "10.0.32.0/19"]
cidr_private_subnets        = ["10.0.128.0/20", "10.0.144.0/20"]
bastion_keypair_public_path = "/home/ubuntu/.ssh/bastion_id_rsa.pub"
app_keypair_public_path     = "/home/ubuntu/.ssh/app_id_rsa.pub"
