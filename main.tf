module "vpc" {
  source               = "./modules/vpc"
  environment          = var.ENVIRONMENT
  cidr_vpc             = var.cidr_vpc
  availability_zones   = var.availability_zones
  cidr_public_subnets  = var.cidr_public_subnets
  cidr_private_subnets = var.cidr_private_subnets
}

module "natgateway" {
  source                  = "./modules/natgateway"
  environment             = var.ENVIRONMENT
  vpc_id                  = module.vpc.vpc_id
  public_subnet_ids       = module.vpc.public_subnet_ids
  private_route_table_ids = module.vpc.private_route_table_ids
}

module "instance" {
  source                              = "./modules/instance"
  environment                         = var.ENVIRONMENT
  vpc_id                              = module.vpc.vpc_id
  private_subnet_ids                  = module.vpc.private_subnet_ids
  db_username                         = var.db_username
  db_user_password                    = var.db_user_password
  db_name                             = var.db_name
  db_endpoint                         = module.db.db_endpoint
  app_keypair_public_path             = var.app_keypair_public_path
  bastion_security_group_id           = module.bastion.bastion_security_group_id
  app_load_balancer_security_group_id = module.loadbalancer.app_load_balancer_security_group_id
  app_target_group_arn                = module.loadbalancer.app_target_group_arn
}

module "loadbalancer" {
  source             = "./modules/loadbalancer"
  environment        = var.ENVIRONMENT
  vpc_id             = module.vpc.vpc_id
  private_subnet_ids = module.vpc.private_subnet_ids
  cidr_vpc           = var.cidr_vpc
}

module "db" {
  source             = "./modules/db"
  environment        = var.ENVIRONMENT
  vpc_id             = module.vpc.vpc_id
  private_subnet_ids = module.vpc.private_subnet_ids
  cidr_vpc           = var.cidr_vpc
  db_username        = var.db_username
  db_user_password   = var.db_user_password
  db_name            = var.db_name
}

module "bastion" {
  source                      = "./modules/bastion"
  environment                 = var.ENVIRONMENT
  vpc_id                      = module.vpc.vpc_id
  public_subnet_ids           = module.vpc.public_subnet_ids
  bastion_keypair_public_path = var.bastion_keypair_public_path
}
