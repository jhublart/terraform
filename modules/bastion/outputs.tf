output "bastion_0" {
  value = [aws_eip.bastion[0].public_ip, aws_eip.bastion[0].public_dns]
}

output "bastion_1" {
  value = [aws_eip.bastion[1].public_ip, aws_eip.bastion[1].public_dns]
}

output "bastion_security_group_id" {
  value = aws_security_group.bastion.id
}