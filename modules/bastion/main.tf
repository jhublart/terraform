# Crée une Data Source aws_ami pour sélectionner l'ami disponible dans la région sélectionnée
data "aws_ami" "bastion_ami" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*"]
  }
}

# Crée les instances EC2 de chaque hôte Bastion
resource "aws_instance" "bastion" {
  count                  = 2
  ami                    = data.aws_ami.bastion_ami.id
  instance_type          = "t2.micro"
  subnet_id              = var.public_subnet_ids[count.index]
  vpc_security_group_ids = [aws_security_group.bastion.id]
  key_name               = aws_key_pair.bastion_keypair.key_name
  tags = {
    Name = "jh_bastion_instance_${var.environment}"
  }
}

# Crée la paire de clés pour la connexion à l'hôte Bastion
resource "aws_key_pair" "bastion_keypair" {
  key_name   = "bastion_keypair_${var.environment}"
  public_key = file(var.bastion_keypair_public_path)
}

/* Crée les Elastic IPs afin de fournir une adresse IP permanente à chaque hôte Bastion.
 * Sans cela, celles-ci pourraient changer lors d'un redémarrage de l'instance.
 */
resource "aws_eip" "bastion" {
  count  = 2
  domain = "vpc"
  tags = {
    Name = "jh_bastion_eip_${var.environment}_${count.index}"
  }
}

# Associe chaque IP élastique à une instance de l'hôte Bastion
resource "aws_eip_association" "bastion" {
  count         = 2
  instance_id   = aws_instance.bastion[count.index].id
  allocation_id = aws_eip.bastion[count.index].id
}