# Env

variable "environment" {
  description = "Nom de l'environnement d'exploitation sélectionné."
  type        = string
}

# Réseau 

variable "vpc_id" {
  description = "ID du VPC."
  type        = any
}

variable "public_subnet_ids" {
  description = ""
  type        = any
}

# Security Group

variable "bastion_sg_ingress_port" {
  description = "Ports ouverts pour le traffic entrant depuis l'extérieur du VPC."
  type        = list(number)
  default     = [22]
}

variable "bastion_sg_egress_port" {
  description = "Ports ouverts pour le traffic sortant vers l'extérieur du VPC."
  type        = list(number)
  default     = [0]
}

# SSH

variable "bastion_keypair_public_path" {
  description = "Chemin d'accès au fichier contenant la clé publique pour la connexion SSH à l'hôte Bastion."
  type        = string
}