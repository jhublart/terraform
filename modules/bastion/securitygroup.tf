# Crée le Security Group pour l'hôte Bastion
resource "aws_security_group" "bastion" {
  vpc_id = var.vpc_id
  dynamic "ingress" {
    for_each = var.bastion_sg_ingress_port
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  dynamic "egress" {
    for_each = var.bastion_sg_egress_port
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  tags = {
    Name = "jh_bastion_sg_${var.environment}"
  }
}