#!/bin/bash

db_username="${db_username}"
db_user_password="${db_user_password}"
db_name="${db_name}"
db_host="${db_host}"
db_port="${db_port}"

sudo yum update -y
sudo yum install -y httpd

sudo yum install -y mysql
sudo mysql -u $db_username -p$db_user_password -h $db_host -P $db_port -e "CREATE DATABASE IF NOT EXISTS $db_name;"

# Active et installe PHP
amazon-linux-extras enable php7.4
sudo yum clean metadata
sudo yum install -y php php-{pear,cgi,common,curl,mbstring,gd,mysqlnd,gettext,bcmath,json,xml,fpm,intl,zip,imap,devel}

# Télécharge et installe WordPress
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz
cp -r wordpress/* /var/www/html/

# Configure WordPress pour utiliser la base de données
cd /var/www/html
cp wp-config-sample.php wp-config.php

sed -i "s/database_name_here/$db_name/g" wp-config.php
sed -i "s/username_here/$db_username/g" wp-config.php
sed -i "s/password_here/$db_user_password/g" wp-config.php
sed -i "s/localhost/$db_host:$db_port/g" wp-config.php

cat <<EOF >> wp-config.php
define( 'FS_METHOD', 'direct' );
define('WP_MEMORY_LIMIT', '256M');
EOF

# Configure les autorisations des fichiers
sudo chown -R apache:apache /var/www
sudo find /var/www -type d -exec chmod 2775 {} \;
sudo find /var/www -type f -exec chmod 0664 {} \;

# Configure Apache
sudo systemctl start httpd
sudo systemctl enable httpd

# Modifie la configuration Apache pour autoriser .htaccess
sudo sed -i '/<Directory "\/var\/www\/html">/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
sudo systemctl restart httpd
