# Crée une Data Source aws_ami pour sélectionner l'ami disponible dans la région sélectionnée
data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*"]
  }
}

# Configure le template de lancement des instances EC2 pour l'AutoScaling Group
resource "aws_launch_configuration" "app" {
  name            = "jh_app_launch_config_${var.environment}"
  image_id        = data.aws_ami.app_ami.id
  instance_type   = "t2.micro"
  user_data       = data.template_file.install_wordpress_app.rendered
  key_name        = aws_key_pair.app_keypair.key_name
  security_groups = [aws_security_group.app.id]
  lifecycle {
    create_before_destroy = true
  }
}

# Crée la paire de clés pour la connexion à l'hôte Bastion
resource "aws_key_pair" "app_keypair" {
  key_name   = "app_keypair_${var.environment}"
  public_key = file(var.app_keypair_public_path)
  tags = {
    Name = "jh_app_keypair_${var.environment}"
  }
}

# Crée le template pour les user data des EC2
data "template_file" "install_wordpress_app" {
  template = file("./modules/instance/install_wordpress_app.sh")
  vars = {
    db_username      = var.db_username
    db_user_password = var.db_user_password
    db_name          = var.db_name
    db_host          = split(":", var.db_endpoint)[0]
    db_port          = split(":", var.db_endpoint)[1]
  }
}

# Crée l'AutoScaling Group prenant en charge les sous-réseaux privés
resource "aws_autoscaling_group" "app" {
  name                      = "jh_app_asg_${var.environment}"
  min_size                  = var.autoscale_min
  max_size                  = var.autoscale_max
  desired_capacity          = var.autoscale_desired
  launch_configuration      = aws_launch_configuration.app.id
  vpc_zone_identifier       = [var.private_subnet_ids[0], var.private_subnet_ids[1]]
  health_check_grace_period = 300
  health_check_type         = "EC2"
  force_delete              = true
  tag {
    key                 = "Name"
    value               = "jh_app_asg_${var.environment}"
    propagate_at_launch = true
  }
}

# Associe l'AutoScaling Group au Load Balancer
resource "aws_autoscaling_attachment" "app" {
  autoscaling_group_name = aws_autoscaling_group.app.id
  lb_target_group_arn    = var.app_target_group_arn
}