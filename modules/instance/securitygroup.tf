# Crée le Security Group pour les instances EC2
resource "aws_security_group" "app" {
  vpc_id = var.vpc_id
  # Traffic provenant de l'AutoScaling Group
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [var.app_load_balancer_security_group_id]
  }
  # Traffic provenant de l'hôte Bastion
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [var.bastion_security_group_id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "jh_app_launch_config_sg_${var.environment}"
  }
}