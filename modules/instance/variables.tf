# Env

variable "environment" {
  description = "Appelle le nom de l'environnement d'exploitation sélectionné."
  type        = string
}

# Réseau

variable "vpc_id" {
  description = "ID du VPC."
  type        = any
}

variable "private_subnet_ids" {
  description = "IDs pour chaque sous-réseau privé."
  type        = list(any)
}

variable "app_keypair_public_path" {
  description = "Chemin d'accès au fichier contenant la clé publique pour la connexion SSH aux instances app."
  type        = string
}

# Base de données

variable "db_username" {
  description = "Indique le nom d'utilisateur wordpress pour la base de données."
  type        = string
}

variable "db_user_password" {
  description = "Indique le mot de passe de l'utilisateur wordpress pour la base de données."
  type        = string
}

variable "db_name" {
  description = "Indique le nom de la base de données."
  type        = string
}

variable "db_endpoint" {
  description = "Endpoint de la base de données."
  type        = any
}

# Security Group

variable "app_load_balancer_security_group_id" {
  description = "ID du Security Group du Load Balancer de l'application."
  type        = any
}

variable "bastion_security_group_id" {
  description = "ID du Security Group de l'hôte Bastion."
  type        = any
}

variable "autoscale_min" {
  description = "Nombre minimal d'instances requis."
  type        = string
  default     = "1"
}

variable "autoscale_max" {
  description = "Nombre maximal d'instances autorisé."
  type        = string
  default     = "2"
}

variable "autoscale_desired" {
  description = "Nombre d'instances désiré."
  type        = string
  default     = "2"
}

variable "app_target_group_arn" {
  description = "Nom des ressources des groupes cibles du Load Balancer."
  type        = any
}