# Configure l'instance de base de données avec Amazon RDS
resource "aws_db_instance" "db" {
  username = var.db_username
  password = var.db_user_password
  db_name = var.db_name
  allocated_storage      = var.allocated_storage
  storage_type           = var.storage_type
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_class
  parameter_group_name   = var.parameter_group_name
  db_subnet_group_name   = aws_db_subnet_group.db.name
  vpc_security_group_ids = [aws_security_group.db.id]
  multi_az               = true
  skip_final_snapshot    = true
  tags = {
    Name = "jh_db_instance_${var.environment}"
  }
}

# Configure le groupe de sous-réseaux associé à la base de données
resource "aws_db_subnet_group" "db" {
  subnet_ids = [var.private_subnet_ids[0], var.private_subnet_ids[1]]
  tags = {
    Name = "jh_db_subnet_group_${var.environment}"
  }
}

