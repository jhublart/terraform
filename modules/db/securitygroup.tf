resource "aws_security_group" "db" {
  vpc_id = var.vpc_id
  ingress {
    from_port   = var.db_ingress_port
    to_port     = var.db_ingress_port
    protocol    = "tcp"
    cidr_blocks = [var.cidr_vpc]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "jh_db_sg_${var.environment}"
  }
}
