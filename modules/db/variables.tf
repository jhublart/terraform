# Env

variable "environment" {
  description = "Nom de l'environnement d'exploitation sélectionné."
  type        = string
}

# Réseau

variable "vpc_id" {
  description = "ID du VPC."
  type        = any
}

variable "private_subnet_ids" {
  description = "IDs pour chaque sous-réseau privé."
  type        = list(any)
}

variable "cidr_vpc" {
  description = "Groupes d'adresses IPs pouvant appeler la base de données."
  type        = any
}

variable "db_ingress_port" {
  description = ""
  type        = number
  default     = 3306
}

# Base de données

variable "db_username" {
  description = "Nom d'utilisateur wordpress pour la base de données."
  type        = string
}

variable "db_user_password" {
  description = "Mot de passe de l'utilisateur wordpress pour la base de données."
  type        = string
}

variable "db_name" {
  description = "Nom de la base de données créée lors de la création de l'instance DB."
  type        = string
}

variable "allocated_storage" {
  description = "Capacité de stockage fournie par AWS."
  type        = number
  default     = 10
}

variable "storage_type" {
  description = "Type de stockage fourni par AWS."
  type        = string
  default     = "gp2" # general purpose SSD
}

variable "instance_class" {
  description = "Type d'instance fourni par AWS pour la base de données."
  type        = string
  default     = "db.t3.micro"
}

variable "engine" {
  description = "Système de gestion de base de données"
  type        = string
  default     = "mysql"
}

variable "engine_version" {
  description = "Version du système de gestion de base de données"
  type        = string
  default     = "5.7"
}

variable "parameter_group_name" {
  description = ""
  type        = string
  default     = "default.mysql5.7"
}
