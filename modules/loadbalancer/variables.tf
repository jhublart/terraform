# Env

variable "environment" {
  description = "Nom de l'environnement d'exploitation sélectionné."
  type        = string
}

# Réseau

variable "vpc_id" {
  description = "ID du VPC."
  type        = any
}

variable "cidr_vpc" {
  description = "Block CIDR correspondant au VPC."
  type        = any
}

variable "private_subnet_ids" {
  description = "IDs pour chaque sous-réseau privé."
  type        = list(any)
}

# Security Group

variable "alb_security_group_ingress_ports" {
  description = "Ports ouverts pour le traffic entrant depuis l'extérieur du VPC."
  type        = list(number)
  default     = [80]
}

variable "alb_security_group_egress_ports" {
  description = "Ports ouverts pour le traffic sortant vers l'extérieur du VPC."
  type        = list(number)
  default     = [0]
}