output "app_load_balancer_security_group_id" {
  value = aws_security_group.alb.id
}

output "app_target_group_arn" {
  value = aws_lb_target_group.alb.arn
}

output "app_load_balancer_dns_name" {
  value = aws_lb.alb.dns_name
}