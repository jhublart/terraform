# Crée un load balancer
resource "aws_lb" "alb" {
  internal = false
  # enable_cross_zone_load_balancing vaut toujours true pour un ALB
  enable_cross_zone_load_balancing = true
  load_balancer_type               = "application"
  subnets                          = [var.private_subnet_ids[0], var.private_subnet_ids[1]]
  security_groups                  = [aws_security_group.alb.id]
  enable_deletion_protection       = false
  tags = {
    Name = "jh_app_load_balancer_${var.environment}"
  }
}

# Crée un écouteur pour l'équilibreur de charge
resource "aws_lb_listener" "alb" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb.arn
  }
  tags = {
    Name = "jh_app_listener_${var.environment}"
  }
}

# Crée un groupe cible
resource "aws_lb_target_group" "alb" {
  load_balancing_cross_zone_enabled = "true"
  port                              = 80
  protocol                          = "HTTP"
  vpc_id                            = var.vpc_id
  tags = {
    Name = "jh_app_target_group_${var.environment}"
  }
}
