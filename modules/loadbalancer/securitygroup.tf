# Crée le Security Group pour l'Application Load Balancer
resource "aws_security_group" "alb" {
  vpc_id = var.vpc_id
  dynamic "ingress" {
    for_each = var.alb_security_group_ingress_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  dynamic "egress" {
    for_each = var.alb_security_group_egress_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  tags = {
    Name = "jh_alb_sg_${var.environment}"
  }
}