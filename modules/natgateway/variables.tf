# Env

variable "environment" {
  description = "Nom de l'environnement d'exploitation sélectionné."
  type        = string
}

# Réseau

variable "vpc_id" {
  description = "ID du VPC."
  type        = any
}

variable "public_subnet_ids" {
  description = "IDs pour chaque sous-réseau public."
  type        = list(any)
}

variable "private_route_table_ids" {
  description = "IDs des tables de routage de chaque sous-réseau privé."
  type        = list(any)
}