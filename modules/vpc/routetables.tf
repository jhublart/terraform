# Crée une table de routage pour chaque sous-réseau public
resource "aws_route_table" "public_rt" {
  count      = 2
  vpc_id     = aws_vpc.vpc.id
  depends_on = [aws_subnet.public_subnet]
  tags = {
    Name = "jh_public_rt_${var.environment}_${count.index}"
  }
}

# Associe tables de routage et sous-réseaux publics correspondant au sein de chaque AZ
resource "aws_route_table_association" "public_rt_public_subnet_association" {
  count          = 2
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public_rt[count.index].id
}

# Crée une table de routage pour chaque sous-réseau privé
resource "aws_route_table" "private_rt" {
  count      = 2
  vpc_id     = aws_vpc.vpc.id
  depends_on = [aws_subnet.private_subnet]
  tags = {
    Name = "jh_private_rt_${var.environment}_${count.index}"
  }
}

# Associe tables de routage et sous-réseaux privés correspondant au sein de chaque AZ
resource "aws_route_table_association" "private_rt_private_subnet_association" {
  count          = 2
  subnet_id      = aws_subnet.private_subnet[count.index].id
  route_table_id = aws_route_table.private_rt[count.index].id
}
