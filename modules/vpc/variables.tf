# Env

variable "environment" {
  description = "Appelle le nom de l'environnement d'exploitation sélectionné."
  type        = string
}

# Réseau

variable "cidr_vpc" {
  description = "CIDR du VPC jh-exam-vpc"
  type        = string
}

variable "availability_zones" {
  description = "Liste les dénomminations des zones de disponibilités sur lesquelles s'étend le VPC."
  type        = list(string)
}

variable "cidr_public_subnets" {
  description = "CIDR des sous-réseaux publics"
  type        = list(string)
}

variable "cidr_private_subnets" {
  description = "CIDR des sous-réseaux privés"
  type        = list(string)
}