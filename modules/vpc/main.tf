# Crée le vpc
resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr_vpc
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "jh_vpc_${var.environment}"
  }
}

# Crée les sous-réseaux publics
resource "aws_subnet" "public_subnet" {
  count                   = 2
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = "true"
  cidr_block              = var.cidr_public_subnets[count.index]
  availability_zone       = var.availability_zones[count.index]
  tags = {
    Name = "jh_public_subnet_${var.environment}_${count.index}"
  }
}

# Crée les sous-réseaux privés
resource "aws_subnet" "private_subnet" {
  count                   = 2
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = "false"
  cidr_block              = var.cidr_private_subnets[count.index]
  availability_zone       = var.availability_zones[count.index]
  tags = {
    Name = "jh_private_subnet_${var.environment}_${count.index}"
  }
}
