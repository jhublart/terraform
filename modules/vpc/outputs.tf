output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "public_subnet_ids" {
  value = [aws_subnet.public_subnet[0].id, aws_subnet.public_subnet[1].id]
}

output "private_subnet_ids" {
  value = [aws_subnet.private_subnet[0].id, aws_subnet.private_subnet[1].id]
}

output "public_route_table_ids" {
  value = [aws_route_table.public_rt[0].id, aws_route_table.public_rt[1].id]
}

output "private_route_table_ids" {
  value = [aws_route_table.private_rt[0].id, aws_route_table.private_rt[1].id]
}