# Crée l'Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "jh_igw_${var.environment}"
  }
}

# Au sein de chaque sous-réseau public, crée la route associant l'Internet Gateway et l'extérieur du VPC
resource "aws_route" "route_igw" {
  count                  = 2
  route_table_id         = aws_route_table.public_rt[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}