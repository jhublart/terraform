# Objectif

L'objectif est de déployer une infrastructure AWS multi-AZ avec Terraform.

Un schéma permettant de visualiser le résultat est disponible - fichier aws-infrastructure.jpg.

# Lancement

En premier lieu, il est nécessaire de créer deux paires de clés SSH - 'ssh-keygen' - pour la connexion distante à l'hôte Bastion et la connexion, interne au VPC, de l'hôte Bastion aux instances 'app'. Celles-ci doivent prendre pour chemin de fichier (possibilité de modifier ces valeurs de chemin dans le fichier terraform.tfvars du dossier principal):
- "/home/ubuntu/.ssh/bastion_id_rsa.pub"
- "/home/ubuntu/.ssh/app_id_rsa.pub"

Ensuite, le script run.sh, situé à la racine du projet, demande les éléments d'authentification AWS et le nom de l'environnement de travail, déploie l'infrastructure et attend l'instruction - 'yes' - avant de détruire celle-ci ou non.

