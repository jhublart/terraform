# Environment variables

variable "AWS_ACCESS_KEY_ID" {
  description = "Clé d'accès pour l'authentification avec AWS."
  type        = string
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "Clé secrète pour l'authentification avec AWS."
  type        = string
}

variable "ENVIRONMENT" {
  description = "Définit l'environnement d'exploitation - dev, qa, staging ou prod - en correspondance avec le workspace sélectionné."
  type        = string
}

# Provider

variable "region" {
  description = "AWS région"
  type        = string
}

# VPC

variable "cidr_vpc" {
  description = "CIDR du VPC jh-exam-vpc"
  type        = string
}

variable "availability_zones" {
  description = "Liste les zones de disponibilités sur lesquelles s'étend le VPC."
  type        = list(string)
}

variable "cidr_public_subnets" {
  description = "CIDR des sous-réseaux publics"
  type        = list(string)
}

variable "cidr_private_subnets" {
  description = "CIDR des sous-réseaux privés"
  type        = list(string)
}

# SSH

variable "bastion_keypair_public_path" {
  description = "Chemin d'accès au fichier contenant la clé publique pour la connexion SSH à l'hôte Bastion."
  type        = string
}

variable "app_keypair_public_path" {
  description = "Chemin d'accès au fichier contenant la clé publique pour la connexion SSH aux instances app."
  type        = string
}

# DB

variable "db_username" {
  description = "Indique le nom d'utilisateur wordpress pour la base de données."
  type        = string
}

variable "db_user_password" {
  description = "Indique le mot de passe de l'utilisateur wordpress pour la base de données."
  type        = string
}

variable "db_name" {
  description = "Indique le nom de la base de données."
  type        = string
}
