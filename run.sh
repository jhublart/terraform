# Propose une coloration pour l'affichage des sorties
NOCOLOR='\033[0m'
BLUE='\033[0;34m'

# Définit le répertoire d'exécution
APP_DIRECTORY="$HOME/terraform"

# Paramètre de la base de données
echo -e "\n${BLUE}Paramétrage de la base de données${NOCOLOR}\n"

echo -e "\n${BLUE}Nom d'utilisateur : ${NOCOLOR}\n"
read db_username

echo -e "\n${BLUE}Mot de passe d'utilisateur : ${NOCOLOR}\n"
read db_user_password

echo -e "\n${BLUE}Nom de la base de données : ${NOCOLOR}\n"
read db_name

# Formate le code Terraform
terraformFormat() {
    cd $APP_DIRECTORY && terraform fmt
    cd ./modules
    cd ../bastion && terraform fmt
    cd ../db && terraform fmt
    cd ../instance && terraform fmt
    cd ../loadbalancer && terraform fmt
    cd ../natgateway && terraform fmt
    cd ../vpc && terraform fmt
}

cd $APP_DIRECTORY

# Initialise le répertoire pour Terraform

terraform init

terraformFormat

cd $APP_DIRECTORY

# Récupère les éléments d'authentification AWS

echo -e "\n${BLUE}AWS ACCESS KEY : ${NOCOLOR}\n"
read awsAccessKey
export TF_VAR_AWS_ACCESS_KEY_ID=$awsAccessKey

echo -e "\n${BLUE}AWS SECRET ACCESS KEY : ${NOCOLOR}\n"
read awsSecretAccessKey
export TF_VAR_AWS_SECRET_ACCESS_KEY=$awsSecretAccessKey

echo -e "\n${BLUE}WORKSPACE : ${NOCOLOR}\n"
read terraformWorkspace
export TF_VAR_ENVIRONMENT=$terraformWorkspace

# Définit l'espace de travail correspondant à l'environnement souhaité

terraform workspace new $TF_VAR_ENVIRONMENT

# Crée l'infrastructure AWS via Terraform

terraform apply -auto-approve -var="db_username=$db_username" -var="db_user_password=$db_user_password" -var="db_name=$db_name"

# Propose de détruire l'infrastructure

echo -e "\n${BLUE}Détruire l'infrastructure via 'terraform destroy -auto-approve' ? (toute autre réponse que 'yes' ignorera cette étape)...${NOCOLOR}\n"
read removeInfra

if [ $removeInfra == "yes" ]
then
    terraform destroy -auto-approve
    terraform workspace select 'default'
    terraform workspace delete "$TF_VAR_ENVIRONMENT"
fi
