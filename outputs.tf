# DNS et IPv4 des hôtes Bastion
output "bastion_0" {
  value = module.bastion.bastion_0
}

output "bastion_1" {
  value = module.bastion.bastion_1
}

# DNS et IPv4 des NAT Gateway
output "nat_gateway_0" {
  value = module.natgateway.nat_gateway_0
}

output "nat_gateway_1" {
  value = module.natgateway.nat_gateway_1
}

# DNS du Load Balancer
output "dns_name" {
  value = module.loadbalancer.app_load_balancer_dns_name
}

# Url de la base de données
output "db_endpoint" {
  value = module.db.db_endpoint
}